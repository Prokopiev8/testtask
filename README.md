Launch .exe with command line arguments
1. Input origin file absolute path
2. Other sample file absolute path
3. ElementId in origin file

Sample:
HtmlCrawler.exe "D:\TestTask\Html\sample-0-origin.html" "D:\TestTask\Html\sample-1-evil-gemini.html" "make-everything-ok-button"

The same attributes will be highlighted with green, the different will be highlighted with red.

In the source code it is possible to set similarityLevel of elements. For this specific task was choosen 60% of the similarity to consider elements as similar.