﻿using System.Collections.Generic;

namespace HtmlCrawler.Model
{
  public class ElementInfo
  {
    public string ElementId;
    public Dictionary<string, string> Attributes;
    public string XPath;
    public double SimilarityLevel;
  }
}
