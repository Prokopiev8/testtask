﻿using HtmlCrawler.Logic;
using System;

namespace HtmlCrawler
{
  class Program
  {
    private static readonly CommandLineArgsParser _commandLineArgsParser = new CommandLineArgsParser();
    private static readonly OriginElementExtractor _originElementExtractor = new OriginElementExtractor();
    private static readonly SimilarElementFinder _similarElementFinder = new SimilarElementFinder(0.6);
    private static readonly ResultViewer _resultViewer = new ResultViewer();

    static void Main(string[] args)
    {
      try
      {
        var commandLineArgs = _commandLineArgsParser.Parse(args);
        var originElementInfo = _originElementExtractor.ExtractOriginElementInfo(commandLineArgs.InputOriginFilePath, commandLineArgs.SelectorElementId);
        var similarElementInfo = _similarElementFinder.FindSimilarElement(originElementInfo, commandLineArgs.InputOtherSampleFilePath);
        _resultViewer.ShowResult(originElementInfo, similarElementInfo);
      }
      catch (Exception ex)
      {
        Console.WriteLine("Error trying to find element by id, Message: {0}", ex.Message);
      }
    }
  }
}
