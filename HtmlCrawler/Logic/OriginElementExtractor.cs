﻿using HtmlAgilityPack;
using HtmlCrawler.Model;
using System;
using System.IO;
using System.Linq;

namespace HtmlCrawler.Logic
{
  public class OriginElementExtractor
  {
    public ElementInfo ExtractOriginElementInfo(string originFilePath, string selectorElementId)
    {
      if (!File.Exists(originFilePath))
        throw new Exception("File not found");

      var document = new HtmlDocument();
      document.Load(originFilePath);

      var element = document.GetElementbyId(selectorElementId);
      if (element == null)
        throw new Exception("Element not found");

      var attributesData = element.Attributes.Where(x => x.Name != "id").ToDictionary(x => x.Name, x => x.Value);
      return new ElementInfo
      {
        ElementId = selectorElementId,
        Attributes = attributesData,
        XPath = element.XPath
      };
    }
  }
}
