﻿using HtmlCrawler.Model;
using System;

namespace HtmlCrawler.Logic
{
  public class ResultViewer
  {
    public void ShowResult(ElementInfo originElementInfo, ElementInfo similarElementInfo)
    {
      if (similarElementInfo == null)
      {
        Console.WriteLine("Element was not found");
        return;
      }

      Console.WriteLine($"Successfully found. XPath: {similarElementInfo.XPath}");
      Console.WriteLine($"SimilarityLevel: {similarElementInfo.SimilarityLevel}");
      Console.WriteLine($"Attributes:");

      foreach (var attribute in similarElementInfo.Attributes)
      {
        Console.ForegroundColor = ConsoleColor.Black;
        if (originElementInfo.Attributes.ContainsKey(attribute.Key) && originElementInfo.Attributes[attribute.Key] == attribute.Value)
          Console.BackgroundColor = ConsoleColor.Green;
        else
          Console.BackgroundColor = ConsoleColor.Red;

        Console.WriteLine($"{attribute.Key}: {attribute.Value}");
      }

      Console.BackgroundColor = ConsoleColor.Black;
      Console.ForegroundColor = ConsoleColor.White;
    }
  }
}
