﻿using HtmlCrawler.Model;
using System;

namespace HtmlCrawler.Logic
{
  public class CommandLineArgsParser
  {
    public CommandLineArgs Parse(string[] args)
    {
      if (args.Length < 3)
        throw new Exception("Not enough args");

      return new CommandLineArgs
      {
        InputOriginFilePath = args[0],
        InputOtherSampleFilePath = args[1],
        SelectorElementId = args[2]
      };
    }
  }
}
