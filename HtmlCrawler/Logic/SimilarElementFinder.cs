﻿using HtmlAgilityPack;
using HtmlCrawler.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HtmlCrawler.Logic
{
  public class SimilarElementFinder
  {
    private double _minSimilarityPercent;

    /// <param name="minSimilarityPercent">Percent of the similarity to consider as similar</param>
    public SimilarElementFinder(double minSimilarityPercent)
    {
      _minSimilarityPercent = minSimilarityPercent;
    }

    public ElementInfo FindSimilarElement(ElementInfo originElement, string inputOtherSampleFilePath)
    {
      if (!File.Exists(inputOtherSampleFilePath))
        throw new Exception("File not found");

      var document = new HtmlDocument();
      document.Load(inputOtherSampleFilePath);

      var nodes = document.DocumentNode.Descendants();
      var element = FindSimilarElementInChildren(nodes.ToList(), originElement);

      return element;
    }

    private ElementInfo FindSimilarElementInChildren(List<HtmlNode> htmlNodes, ElementInfo originElement)
    {
      foreach (var node in htmlNodes) 
      {
        var elementInfo = new ElementInfo
        {
          Attributes = node.Attributes.ToDictionary(x => x.Name, x => x.Value),
          XPath = node.XPath
        };

        var similarityLevel = GetSimilarityLevel(originElement, elementInfo);
        elementInfo.SimilarityLevel = similarityLevel;
        if (similarityLevel >= _minSimilarityPercent)
          return elementInfo;
      }

      return null;
    }

    private double GetSimilarityLevel(ElementInfo originElement, ElementInfo element)
    {
      int similarElementsCount = 0;
      foreach (var attribute in originElement.Attributes)
      {
        if (element.Attributes.ContainsKey(attribute.Key) && element.Attributes[attribute.Key] == attribute.Value)
          similarElementsCount++;
      }

      var similarityLevel = similarElementsCount / (double)originElement.Attributes.Count;
      return similarityLevel;
    }
  }
}
