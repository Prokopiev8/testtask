﻿namespace HtmlCrawler.Model
{
  public class CommandLineArgs
  {
    public string InputOriginFilePath;
    public string InputOtherSampleFilePath;
    public string SelectorElementId;
  }
}
